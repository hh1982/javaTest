package www.baidu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
//今天加一行
public class QQServer implements Runnable {
	static ServerSocket server;
	Socket Client = null;
	BufferedReader buf = null;
	static List<Socket> socketList = new LinkedList<Socket>();

	public QQServer(Socket s) {
		Client = s;
		socketList.add(s);
	}

	public void run() {
		try {
			buf = new BufferedReader(new InputStreamReader(
					Client.getInputStream()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean text;
		try {
			while (text = buf.readLine() != null) {
				Iterator<Socket> it = socketList.iterator();
				Socket s = it.next();
				if (s != Client) {
					PrintWriter pw = new PrintWriter(Client.getOutputStream());
					pw.println(text);
					pw.flush();
				}

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void main(String[] args) throws IOException {
		server = new ServerSocket(3000);
		while (true) {
			Socket s = server.accept();
			Thread th = new Thread(new QQServer(s));
		}
	}

}
